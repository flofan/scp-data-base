const Discord = require('discord.js');
const client = new Discord.Client();
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

client.on('ready', () => {
  console.log(`Bot connecté en tant que ${client.user.tag} !`);
  client.user.setActivity("les nouveaux rapports. | !database pour plus d'info", {type: "WATCHING"})
  client.user.setStatus('online')
  client.user.setAvatar("avatar.png")
});

client.on('message', function (msg){
  const args = msg.content.trim().split(/ +/g)
  if(args[0] === "!database"){
      if(args[1] === "scp"){
      const embedstart = new Discord.RichEmbed()
      .setAuthor("SCP DataBase")
      .setDescription("Recherche de votre SCP :")
      .addField("Chiffrement des données terminées", "Connexion à la base de donnée en cours...")
      client.user.setActivity('Recherche le SCP ' + args[2], {type: "WATCHING"})
      client.user.setStatus('dnd')
      msg.channel.send(embedstart).then((msg)=>{
        const request = new XMLHttpRequest();
        request.open('GET', "http://fondationscp.wikidot.com/scp-" + args[2])
        request.send(null)
        request.onreadystatechange = function(){
        if(request.readyState === 4)
        {
          if(request.status === 404) {
            const embed = new Discord.RichEmbed()
            .setAuthor("SCP DataBase")
            .setDescription("Recherche de votre SCP :")
            .addField("ERREUR 404", "Le SCP que vous cherchez n'existe pas.")
            .setColor([255, 0, 0])
            msg.edit(embed)
            client.user.setActivity("les nouveaux rapports. | !database pour plus d'info", {type: "WATCHING"})
            client.user.setStatus('online')
            return
        }else{
            const embedafter = new Discord.RichEmbed()
            .setAuthor("SCP DataBase")
            .setDescription("Recherche de votre SCP :")
            .addField("Connexion réussi.", "Accréditation de niveau OMNI \n Docteur [OùNi ErrOR]\n")
            .addField("Résultat de la recherche : " + args[2], "http://fondationscp.wikidot.com/scp-" + args[2])
            .setColor([0, 100, 0])
            msg.edit(embedafter)
            client.user.setActivity("les nouveaux rapports. | !database pour plus d'info", {type: "WATCHING"})
            client.user.setStatus('online')
            }
          }
        }
      })
    }else if(args[1] === "search"){
      var compt = 2
      var stringtable = new Array()
      while(compt !== args.length)
      {
        stringtable.push(args[compt])
        compt++
      }
      const embedstart = new Discord.RichEmbed()
      .setAuthor("SCP DataBase")
      .setDescription("Recherche dans la base de donnée :")
      .addField("Chiffrement des données terminées", "Connexion à la base de donnée en cours...")
      client.user.setActivity('Recherche ' + stringtable, "STREAMING")
      client.user.setStatus('dnb')
      msg.channel.send(embedstart).then((msg)=>{
        const request = new XMLHttpRequest();
        request.open('GET', "http://fondationscp.wikidot.com/search:site/a/p/q/" + stringtable.join(" "))
        request.send(null)
        request.onreadystatechange = function(){
          if(request.readyState === 4){
            if(request.status === 404) {
              const embed = new Discord.RichEmbed()
              .setAuthor("SCP DataBase")
              .setDescription("Recherche dans la base de donnée :")
              .addField("ERREUR 404", "Aucun résultat n'a été trouvé.")
              .setColor([255, 0, 0])
              msg.edit(embed)
              client.user.setActivity("les nouveaux rapports. | !database pour plus d'info",{type: "WATCHING"})
              client.user.setStatus('online')
              return
            }else{
              const embedafter = new Discord.RichEmbed()
            .setAuthor("SCP DataBase")
            .setDescription("Recherche dans la base de donnée  :")
            .addField("Connexion réussi.", "Accréditation de niveau OMNI \n Docteur [OùNi ErrOR]\n")
            .addField("Résultat de la recherche : " + stringtable.join(" "), "http://fondationscp.wikidot.com/search:site/a/p/q/" + stringtable.join("%20"))
            .setColor([0, 100, 0])
            msg.edit(embedafter)
            client.user.setActivity("les nouveaux rapports. | !database pour plus d'info",{type: "WATCHING"})
            client.user.setStatus('online')
            }
          }
        } 
      })
    }else if(args[1] === "other"){
      switch(args[2]){
        case "mtf":
        const embedmtf = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Force d'Intervention Mobile")
        .addField("Résultat", "http://fondationscp.wikidot.com/task-forces")
        .setColor([99, 59, 59])
        msg.channel.send(embed)
        //http://fondationscp.wikidot.com/task-forces
          break;
        case "introduction":
        const embedintro = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : A propos de la Fondation")
        .addField("Résultat", "http://fondationscp.wikidot.com/about-the-scp-foundation")
        .setColor([99, 59, 59])
        msg.channel.send(embedintro)
        //http://fondationscp.wikidot.com/about-the-scp-foundation
          break;
        case "object_class":
        const embedobj = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Classe des Objets")
        .addField("Résultat", "http://fondationscp.wikidot.com/object-classes")
        .setColor([99, 59, 59])
        msg.channel.send(embedobj)
        //http://fondationscp.wikidot.com/object-classes
          break;
        case "menace_level":
        const embedlevel = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Niveau de Menace")
        .addField("Résultat", "http://fondationscp.wikidot.com/niveaux-de-menace-des-objets-scp")
        .setColor([99, 59, 59])
        msg.channel.send(embedlevel)
        //http://fondationscp.wikidot.com/niveaux-de-menace-des-objets-scp
          break;
        case "security_clearance_level":
        const embedsecu = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Sécurité et Accréditation")
        .addField("Résultat", "http://fondationscp.wikidot.com/security-clearance-levels")
        .setColor([99, 59, 59])
        msg.channel.send(embedsecu)
        //http://fondationscp.wikidot.com/security-clearance-levels
          break;
        case "site_list":
        const embedlist = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Liste des Entrepôts")
        .addField("Résultat", "http://fondationscp.wikidot.com/secure-facilities-locations")
        .setColor([99, 59, 59])
        msg.channel.send(embedlist)
        //http://fondationscp.wikidot.com/secure-facilities-locations
          break;
        case "interest_group":
        const embedgroup = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Groupes d'Intérêts")
        .addField("Résultat", "http://fondationscp.wikidot.com/secure-facilities-locations")
        .setColor([99, 59, 59])
        msg.channel.send(embedgroup)
        //http://fondationscp.wikidot.com/groups-of-interest
          break;
        case "lockdown":
        const embedld = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Procédures de Verouillage")
        .addField("Résultat", "http://fondationscp.wikidot.com/lockdown-procedures")
        .setColor([99, 59, 59])
        msg.channel.send(embedld)
        //http://fondationscp.wikidot.com/lockdown-procedures
          break;
        case "o5":
        const embedo5 = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Dossier Commandement O5")
        .addField("Résultat", "http://fondationscp.wikidot.com/o5-command-dossier")
        .setColor([99, 59, 59])
        msg.channel.send(embedo5)
        //http://fondationscp.wikidot.com/o5-command-dossier
          break;
        case "mce":
        const embedmce = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Introduction au Comité d'Éthique ")
        .addField("Résultat", "http://fondationscp.wikidot.com/ethics-committee-orientation")
        .setColor([99, 59, 59])
        msg.channel.send(embedmce)
        //http://fondationscp.wikidot.com/ethics-committee-orientation
          break;
        case "departments":
        const embeddepart = new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Départements Internes ")
        .addField("Résultat", "http://fondationscp.wikidot.com/list-of-foundation-s-internal-departments")
        .setColor([99, 59, 59])
        msg.channel.send(embeddepart)
        //http://fondationscp.wikidot.com/list-of-foundation-s-internal-departments
          break;
        case "scenario":
        msg.channel.send(new Discord.RichEmbed()
        .setAuthor("SCP Database")
        .setDescription("Page demandée : Scenario -K de la Fondation SCP (NON OFFICIEL)")
        .addField("Résultat", "http://fondationscp.wikidot.com/liste-non-officielle-des-scenarii-k")
        .setColor([99, 59, 59]))
        break;
        case "list":
          msg.channel.sendMessage(new Discord.RichEmbed()
          .setAuthor('SCP Database')
          .setDescription('Liste des IDs des pages (exclusif au bot)')
          .addField('mtf', "Force d'Intervention Mobile (aka FIM)")
          .addField('introduction', "A Propos de la Fondation")
          .addField('object_class', "Classes des Objets")
          .addField('menace_level', "Niveaux de Menace")
          .addField('security_clearance_level', "Sécurité et Accréditation")
          .addField('site_list', "Liste des Entrepôts")
          .addField('interest_group', "Groupes d'intérêts")
          .addField('lockdown', "Procédure de Verouillage")
          .addField('o5', "Dossier Commandement O5")
          .addField('mce', "Introduction au Comité d'Ethique")
          .addField('departments', "Départements Internes")
          .addField('scenario', "Scenario -K de la Fondation SCP (NON OFFICIEL)")
          .setColor([99, 59, 59]))
          break;
      }
    }else{
      msg.channel.send(new Discord.RichEmbed()
      .setAuthor('SCP Database')
      .setDescription("Ce bot permet d'avoir un accès à la base de donnée de la Fondation sur votre serveur.")
      .addField("Commande pour rechercher un scp", 'Usage : `!database scp <Matricule du SCP>`\n Exemple : `!database scp 079`')
      .addBlankField()
      .addField("Commande pour rechercher une page", 'Usage : `!database search <ce que vous voulez cherchez sur la base de donnée>`\n Exemple : `!database search Ogive Omega`')
      .addBlankField()
      .addField("Commande pour avoir des pages rapidement", 'Usage : `!database other <id | list>`\n Exemples : `!database other list` ou `!database other mtf`')
      .setFooter("Ce bot a été crée par Flo - Fan.")
      .setColor([99, 59, 59]))
    }
  }
})

client.login('Mzc1MjE2MDYyNjA3NjU0OTEy.D0Bj0Q.t1A3pcNkyxmX7Ccvqc3QWSNVKcw');